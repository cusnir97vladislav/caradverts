import React, {useContext, useState} from 'react';
import {initialState} from '../commons/utils/dates';

const AppContext = React.createContext([{}, () => {}]);

export const useAppState = () => {
  return useContext(AppContext);
};

export const AppProvider = ({children}) => {
  const [data, setData] = useState(initialState);

  return (
    <AppContext.Provider value={[data, setData]}>
      {children}
    </AppContext.Provider>
  );
};
