// Grayscale
export const gray = '#E5E5E5';
export const gray_light = '#F2F2F2';
export const gray_dark = '#6D6D6D';

// Bluescale
export const blue = '#3CBBEC';
export const blue_light = '#A2E1F2';
export const blue_medium = '#27324D';
export const blue_dark = '#2c2a40';

// DEFAULT
export const white = '#FFFFFF';
export const red = '#EB2122';
export const black = '#000000';

export const primaryDark = blue_dark;
export const primaryDefault = blue;
export const primaryLight = blue_light;

export const primaryText = blue_dark;
export const secondaryText = gray;
export const background = gray;
export const onBackground = blue_light;

// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const ALERT = red;

export const pickerColors = [
  '#f3a683',
  '#f7d794',
  '#778beb',
  '#e77f67',
  '#cf6a87',
  '#f19066',
  '#f5cd79',
  '#546de5',
  '#e15f41',
  '#c44569',
  '#786fa6',
  '#f8a5c2',
  '#63cdda',
  '#ea8685',
  '#596275',
  '#574b90',
  '#f78fb3',
  '#3dc1d3',
  '#e66767',
  '#303952',
  '#55efc4',
  '#00b894',
  '#ffeaa7',
  '#fdcb6e',
  '#81ecec',
  '#00cec9',
  '#fab1a0',
  '#e17055',
  '#74b9ff',
  '#0984e3',
  '#ff7675',
  '#d63031',
  '#a29bfe',
  '#6c5ce7',
  '#fd79a8',
  '#e84393',
  '#dfe6e9',
  '#b2bec3',
  '#636e72',
  '#2d3436'
];
