import {scaleFont} from './mixins';

// FONT WEIGHT
export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_SEMIBOLD = '600';

// FONT SIZE
export const FONT_SIZE = scaleFont(16);
export const FONT_SIZE_28 = scaleFont(28);
