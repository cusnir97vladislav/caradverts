import {scaleSize} from './mixins';
export const COLOR_PICKER_SIZE = scaleSize(35);
export const ICON_SIZE = scaleSize(15);
export const INNER_MARGIN = 8;
export const OUTER_MARGIN = 16;


