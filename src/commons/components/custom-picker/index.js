import React from 'react';
import CustomSelect from '../custom-select';
import {ScrollView, TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
import * as sizes from '../../sizes';
import * as colors from '../../colors';
import * as typography from '../../typography';
import {pickerColors} from '../../colors';
import {scaleFont} from '../../mixins';
import Icon from 'react-native-vector-icons/FontAwesome';

const CustomPicker = (props) => {
  return (
    <CustomSelect
      title="Color"
      backgroundColor={props.value}
      validation={props.validation}
      valueDepends="backgroundColor">
      <PickerContainer {...props} />
    </CustomSelect>
  );
};

const PickerContainer = ({value, onSelect, onClose}) => {
  const handleOnPress = (backgroundColor) => () => onSelect(backgroundColor);

  return (
    <Container>
      <ColorPickerContainer>
        <ColorPickerHeader>
          <TouchableOpacity onPress={onClose} activeOpacity={0.7}>
            <ColorPickerButtonText>Done</ColorPickerButtonText>
          </TouchableOpacity>
        </ColorPickerHeader>
        <ScrollView>
          <ColorsBox>
            {pickerColors.map((backgroundColor) => {
              return (
                <TouchableOpacity
                  key={backgroundColor}
                  activeOpacity={0.7}
                  onPress={handleOnPress(backgroundColor)}>
                  <PickerColorBox
                    isSelected={value === backgroundColor}
                    backgroundColor={backgroundColor}
                  />
                </TouchableOpacity>
              );
            })}
          </ColorsBox>
        </ScrollView>
      </ColorPickerContainer>
    </Container>
  );
};

function PickerColorBox({backgroundColor, isSelected}) {
  return (
    <ColorBoxContainer backgroundColor={backgroundColor}>
      {isSelected && (
        <ColorBoxSelected>
          <Icon
            name="check"
            size={sizes.ICON_SIZE}
            color={colors.primaryLight}
          />
        </ColorBoxSelected>
      )}
    </ColorBoxContainer>
  );
}

export default CustomPicker;

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  background-color: rgba(0, 0, 0, 0.7);
`;

const ColorPickerContainer = styled.View`
  max-height: 50%;
  width: 100%;
  background-color: ${colors.gray_light};
`;
const ColorPickerHeader = styled.View`
  justify-content: flex-end;
  flex-direction: row;
  align-items: center;
  padding: 15px 10px;
`;
const ColorPickerButtonText = styled.Text`
  font-size: ${scaleFont(16)}px;
  font-weight: ${typography.FONT_WEIGHT_SEMIBOLD};
  color: ${colors.blue_dark};
`;
const ColorsBox = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`;
const ColorBoxContainer = styled.View`
  position: relative;
  ${({backgroundColor}) =>
    backgroundColor && `background-color: ${backgroundColor}`}
  margin: 10px;
  width: ${sizes.COLOR_PICKER_SIZE}px;
  height: ${sizes.COLOR_PICKER_SIZE}px;
  border-radius: 10px;
  overflow: hidden;
`;
const ColorBoxSelected = styled.View`
  position: absolute;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5),
  justify-content: center;
  align-items: center;
`;
