import React, {useState} from 'react';
import ImagePicker from 'react-native-image-picker';
import styled from 'styled-components/native';
import {ActivityIndicator, Image, Text, Alert} from 'react-native';
import * as colors from '../../colors';
import {scaleSize} from '../../mixins';
import * as sizes from '../../sizes';
import Icon from 'react-native-vector-icons/FontAwesome';
Icon.loadFont();

const CustomImagePicker = ({title, value, onChange, validation}) => {
  const [isLoading, setLoading] = useState(false);

  let options = {
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const onPress = () => {
    setLoading((prevState) => true);
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let lastId =
          value.length > 0 ? [...[value[value.length - 1]]][0].id : 0;
        let id = lastId + 1;
        const source = {id, uri: response.uri};
        const arrArray = [...value, source];

        onChange(arrArray);
      }
      setLoading((prevState) => false);
    });
  };

  const deleteImage = (imageId) => {
    const deleteFromArray = () => {
      const newArray = value.filter(({id}) => id !== imageId);
      onChange(newArray);
    };

    Alert.alert(
      'Delete image',
      'Are you sure to delete?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => deleteFromArray()},
      ],
      {cancelable: false},
    );
  };

  return (
    <Container>
      <ContainerButton>
        <Text>{title}</Text>
        <Button onPress={onPress}>
          <Icon name="plus" size={sizes.ICON_SIZE} color={colors.blue_dark} />
          <Icon
            name="file-image-o"
            size={sizes.ICON_SIZE * 2}
            color={colors.blue_dark}
          />
        </Button>
      </ContainerButton>
      {isLoading ? <ActivityIndicator size="small" color="#0000ff" /> : null}
      <ContainerImages>
        {value.map(({id, uri}) => (
          <ImageBox key={id} onPress={() => deleteImage(id)}>
            <UpImage source={{uri}} />
            <Overlay>
              <Icon
                name="trash-o"
                size={sizes.ICON_SIZE}
                color={colors.white}
              />
            </Overlay>
          </ImageBox>
        ))}
      </ContainerImages>
    </Container>
  );
};

export default CustomImagePicker;

const Container = styled.View``;

const ContainerButton = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: ${colors.white};
  border-radius: ${scaleSize(10)}px;
  margin: 10px;
  padding: 10px;
`;

const ContainerImages = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  background-color: ${colors.white};
  border-radius: ${scaleSize(10)}px
  margin: 5px 10px;
`;

const ImageBox = styled.TouchableOpacity`
  position: relative;
  width: 50px;
  height: 50px;
  margin: 10px;
  border-radius: 10px;
  overflow: hidden;
`;

const UpImage = styled(Image)`
  width: 100%;
  height: 100%;
`;

const Overlay = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.2);
  justify-content: center;
  align-items: center;
`;

const Button = styled.TouchableOpacity`
  flex-grow: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
