import * as React from 'react';
import styled from 'styled-components/native';
import * as typography from '../../typography';
import * as colors from '../../colors';
import * as sizes from '../../sizes';
import {scaleSize} from '../../mixins';

function List({items, onPress}) {
  return (
    <ListContainer>
      {items.map(({id, title}) => (
        <ListRow key={id} title={title} onPress={() => onPress(id)} />
      ))}
    </ListContainer>
  );
}

function ListRow({title, ...rest}) {
  return (
    <ListRowContainer {...rest} activeOpacity={0.7}>
      <ListRowWrapper>
        <ListRowTitle>{title}</ListRowTitle>
      </ListRowWrapper>
    </ListRowContainer>
  );
}

List.Row = ListRow;

export default List;

const ListContainer = styled.ScrollView`
  flex-grow: 0;
`;

const ListRowContainer = styled.TouchableOpacity`
  background-color: ${colors.white};
  margin: ${scaleSize(sizes.INNER_MARGIN)}px 10px;
  border-radius: 10px;
`;

const ListRowWrapper = styled.View`
  padding: ${scaleSize(20)}px;
`;

const ListRowTitle = styled.Text`
  color: ${colors.primaryText};
  font-size: ${typography.FONT_SIZE_28}px;
  font-weight: ${typography.FONT_WEIGHT_SEMIBOLD};
`;
