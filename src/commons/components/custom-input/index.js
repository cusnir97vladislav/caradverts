import React from 'react';
import styled from 'styled-components/native';
import * as colors from '../../colors';
import {scaleFont, scaleSize} from '../../mixins';

const CustomInput = (props) => {
  const style = props.multiline ? {textAlignVertical: 'top'} : '';

  return <Input {...props} style={style} />;
};

const Input = styled.TextInput.attrs({
  placeholderTextColor: colors.blue_dark,
})`
  font-size: ${scaleFont(16)}px;
  color: ${colors.blue_dark};
  background-color: ${colors.white};
  border-width: 1px;
  border-color: transparent;
  border-radius: ${scaleSize(10)}px;
  padding: ${scaleSize(12)}px;
  margin: 10px;
  ${({validation}) => validation && `border-color: ${colors.red}; `}
`;

export default CustomInput;
