import * as React from 'react';
import {Platform, TouchableOpacity, Modal} from 'react-native';
import {useState} from 'react';
import styled from 'styled-components/native';
import * as colors from '../../colors';
import * as sizes from '../../sizes';
import * as typography from '../../typography';
import {Picker} from '@react-native-community/picker';
import {scaleFont, scaleSize} from '../../mixins';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CustomSelect = (props) => {
  const {
    data,
    onSelect,
    value,
    title = 'Select',
    onSelectValueChange,
    disabled,
    children,
    backgroundColor,
    validation,
    valueDepends,
  } = props;
  const [toggle, setToggle] = useState(false);
  const selectTitle = value ? value : title;

  const onClose = () => {
    (value || props[valueDepends]) && setToggle(false);
  };

  const content = children ? (
    <Modal transparent visible={toggle}>
      <SelectModalContainer>
        {React.cloneElement(children, {onClose})}
      </SelectModalContainer>
    </Modal>
  ) : (
    <CustomPicker
      value={value}
      data={data}
      visible={toggle}
      onSelect={onSelect}
      onClose={onClose}
      onSelectValueChange={onSelectValueChange}
      disabled={disabled}
    />
  );

  return (
    <SelectContainer>
      <Select
        disabled={disabled}
        onPress={() => {
          setToggle(true);
        }}
        activeOpacity={1}
        backgroundColor={backgroundColor}
        validation={validation}>
        <SelectTitle disabled={disabled} backgroundColor={backgroundColor}>
          {selectTitle}
        </SelectTitle>
        <SelectIcon>
          <Ionicons
            name="chevron-down"
            size={sizes.ICON_SIZE}
            color={
              backgroundColor || disabled ? colors.white : colors.blue_dark
            }
          />
        </SelectIcon>
        {content}
      </Select>
    </SelectContainer>
  );
};

const IosPicker = (props) => {
  const {value, data, onSelectValueChange, visible, onClose, disabled} = props;
  return (
    <Modal transparent visible={visible}>
      <IosContainer>
        <IosPickerContainer>
          <IosHeader>
            <TouchableOpacity onPress={onClose} activeOpacity={0.7}>
              <IosButtonText>Done</IosButtonText>
            </TouchableOpacity>
          </IosHeader>
          <Picker
            enabled={!disabled}
            selectedValue={value}
            onValueChange={(itemValue) => {
              onSelectValueChange(itemValue);
            }}>
            {data.map((item) => (
              <Picker.Item key={item + '1'} label={item} value={item} />
            ))}
          </Picker>
        </IosPickerContainer>
      </IosContainer>
    </Modal>
  );
};

const AndroidPicker = ({value, data, onSelectValueChange, disabled}) => {
  return (
    <AndroidPickerContainer
      enabled={!disabled}
      selectedValue={value}
      onValueChange={(itemValue) => {
        onSelectValueChange(itemValue);
      }}>
      {data.map((item) => (
        <Picker.Item key={item + '1'} label={item} value={item} />
      ))}
    </AndroidPickerContainer>
  );
};

const CustomPicker = (props) => {
  return (
    <CustomPickerContainer>
      {Platform.OS === 'ios' ? (
        <IosPicker {...props} />
      ) : (
        <AndroidPicker {...props} />
      )}
    </CustomPickerContainer>
  );
};

export default CustomSelect;

const SelectContainer = styled.View`
  position: relative;
  margin: 10px;
`;
const Select = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: ${({disabled, backgroundColor}) => {
    if (disabled) {
      return 'rgba(0, 0, 0, 0.1)';
    } else if (backgroundColor) {
      return backgroundColor;
    } else {
      return colors.white;
    }
  }};
  border-radius: ${scaleSize(10)}px;
  padding: ${scaleSize(12)}px;
  border-width: 1px;
  border-color: transparent;
  ${({validation}) => validation && `border-color: ${colors.red}; `}
`;
const SelectTitle = styled.Text`
  font-size: ${scaleFont(16)}px;
  color: ${({disabled, backgroundColor}) =>
    disabled || backgroundColor ? colors.white : colors.blue_dark};
`;
const SelectIcon = styled.View``;

const SelectModalContainer = styled.View`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
`;

const CustomPickerContainer = styled.View`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
`;
const IosContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  background-color: rgba(0, 0, 0, 0.7);
`;
const IosPickerContainer = styled.View`
  height: ${scaleSize(250)}px
  width: 100%;
  background-color: ${colors.gray_light};
`;
const IosHeader = styled.View`
  justify-content: flex-end;
  flex-direction: row;
  align-items: center;
  padding: 15px 10px;
`;
const IosButtonText = styled.Text`
  font-size: ${scaleFont(16)}px;
  font-weight: ${typography.FONT_WEIGHT_SEMIBOLD};
  color: ${colors.blue_dark};
`;
const AndroidPickerContainer = styled(Picker)`
  position: absolute;
  background-color: transparent;
  color: transparent;
  width: 100%;
  height: 100%;
`;
const Container = styled.SafeAreaView``;
