import * as React from 'react';
import styled from 'styled-components/native';
import * as typography from '../../../commons/typography';
import * as colors from '../../../commons/colors';
import * as sizes from '../../../commons/sizes';
import {scaleSize} from '../../mixins';

function ScreenHeader({title}) {
  return (
    <Container>
      <Title>{title}</Title>
    </Container>
  );
}

export default ScreenHeader;

const Container = styled.View`
  padding: ${scaleSize(sizes.INNER_MARGIN * 2)}px;
`;

const Title = styled.Text`
  color: ${colors.primaryText};
  font-size: ${typography.FONT_SIZE_28}px;
  font-weight: ${typography.FONT_WEIGHT_SEMIBOLD};
`;
