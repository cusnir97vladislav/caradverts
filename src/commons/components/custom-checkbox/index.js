import React from 'react';
import CheckBox from '@react-native-community/checkbox';
import styled from 'styled-components/native';
import * as colors from '../../colors';
import {scaleSize} from '../../mixins';

const CustomCheckBox = ({title, value, onChange}) => {
  return (
    <Container>
      <CheckBox disabled={false} value={value} onValueChange={onChange} />
      <Title>{title}</Title>
    </Container>
  );
};

export default CustomCheckBox;

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: ${colors.white};
  border-radius: ${scaleSize(10)}px;
  margin: 10px;
  padding: ${scaleSize(7)}px;
`;

const Title = styled.Text`
  margin-left: 10px;
`;
