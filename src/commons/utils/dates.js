export const initialState = [
  {
    id: 1,
    title: 'Peugeot 2010',
    brand: 'Peugeot',
    model: '107',
    fuel: 'diesel',
    color: '#3dc1d3',
    damage: false,
    damageDescription: '',
    amageImages: [],
  },
];

export const fuelType = [
  'petrol',
  'diesel',
  'LPG',
  'hybrid',
  'electric',
  'other',
];
