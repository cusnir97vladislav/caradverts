export const fetchData = (url) => {
  return fetch(url).then((response) => response.json());
};

export const extractDataByKey = (data, keyObj) => {
  return data.map((item) => item[keyObj]);
};

export const getByID = (data, itemId) => {
  return data.filter(({id}) => id == itemId);
};
