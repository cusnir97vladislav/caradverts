import * as React from 'react';
import NavigationStackScreen from './navigation/navigation-stack-screen';
import {AppProvider} from './config/app-provider';

const App = () => (
  <AppProvider>
    <NavigationStackScreen />
  </AppProvider>
);

export default App;
