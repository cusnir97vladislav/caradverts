import * as React from 'react';
import ScreenHeader from '../../commons/components/screen-header';
import List from '../../commons/components/list';
import styled from 'styled-components/native';
import * as colors from '../../commons/colors';
import {useAppState} from '../../config/app-provider';

const Home = ({navigation}) => {
  const [data] = useAppState();

  const onPressListItem = (id) => {
    navigation.navigate('Details', {
      itemId: id,
    });
  };

  return (
    <Container>
      <ScreenHeader title="Car Adverts" />
      <List items={data} onPress={onPressListItem} />
    </Container>
  );
};

export default Home;

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${colors.background};
`;
