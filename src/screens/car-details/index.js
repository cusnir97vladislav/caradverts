import * as React from 'react';
import {Button, Text} from 'react-native';
import styled from 'styled-components/native';
import {useAppState} from '../../config/app-provider';
import {getByID} from '../../commons/utils/helpers';

const CarDetails = ({route, navigation}) => {
  const [setApp, setAppData] = useAppState();
  const {itemId} = route.params;
  const carData = getByID(setApp, itemId);
  const testCarData = JSON.stringify(carData);
  return (
    <Container>
      <Text>Car details for: {itemId}</Text>
      <Text>Car details for: {testCarData}</Text>
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </Container>
  );
};

export default CarDetails;

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 50px;
`;
