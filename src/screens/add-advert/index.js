import * as React from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import {useEffect, useMemo, useState} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {extractDataByKey, fetchData} from '../../commons/utils/helpers';
import {useAppState} from '../../config/app-provider';
import styled from 'styled-components/native';
import * as colors from '../../commons/colors';
import FormikForm from './formik-form';

const AddFormSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, 'Too Short!')
    .max(80, 'Too Long!')
    .required('Required'),
  brand: Yup.string().required('Required'),
  model: Yup.string().required('Required'),
  fuel: Yup.string().required('Required'),
  color: Yup.string().required('Required'),
  damage: Yup.boolean(),
  damageDescription: Yup.string().when('damage', {
    is: true,
    then: Yup.string()
      .min(2, 'Too Short!')
      .max(5000, 'Too Long!')
      .required('Required'),
  }),
  damageImages: Yup.array().when('damage', {
    is: true,
    then: Yup.array().max(3, 'Max 3 Images'),
  }),
});

const AddAdvert = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [_, setAppData] = useAppState();

  const URL =
    'https://raw.githubusercontent.com/matthlavacka/car-list/master/car-list.json';

  useEffect(() => {
    fetchData(URL)
      .then((carsData) => {
        const carsBrand = extractDataByKey(carsData, 'brand');
        setData({carsData, carsBrand});
      })
      .catch((error) => console.log(error))
      .finally(() => setLoading(false));
  }, []);

  const AddAdvertForm = () => {
    const onSubmit = (values) => {
      setAppData((prevState) => {
        let lastId = [...[prevState[prevState.length - 1]]][0].id;
        let id = lastId + 1;
        const newValues = {id, ...values};
        return [...prevState, ...[newValues]];
      });
      navigation.goBack();
    };

    const formikInitialValues = {
      title: '',
      brand: '',
      model: '',
      fuel: '',
      color: '',
      damage: false,
      damageDescription: '',
      damageImages: [],
    };

    return (
      <Formik
        initialValues={formikInitialValues}
        validationSchema={AddFormSchema}
        onSubmit={onSubmit}>
        {(props) => <FormikForm {...props} data={data} />}
      </Formik>
    );
  };

  return (
    <Container>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="small" color="#0000ff" />
        </View>
      ) : (
        <AddAdvertForm />
      )}
    </Container>
  );
};

export default AddAdvert;

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${colors.background};
`;
