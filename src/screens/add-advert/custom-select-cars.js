import React from 'react';
import {useMemo, useState} from 'react';
import FormikForm from './formik-form';
import CustomSelect from '../../commons/components/custom-select';

const CustomSelectCars = (props) => {
  const {
    carsData,
    carsBrand,
    brandValue,
    modelValue,
    onSelectBrand,
    onSelectModel,
    validation: {errors, touched},
  } = props;

  const [carModels, setCarModels] = useState([]);

  const extractCarModels = (carBrand) => {
    const {models} = carsData.find(({brand}) => brand === carBrand);
    return models;
  };

  const handleChangeCarModel = (model) => {
    onSelectModel(model);
  };

  const handleChangeCarsBrand = (brand) => {
    const carModels = extractCarModels(brand);
    onSelectModel(carModels[0]);
    setCarModels(carModels);
    onSelectBrand(brand);
  };

  const selectTitle = (initialValue, value) => {
    return value !== '' ? value : initialValue;
  };

  const disableSelect = (dependent) => {
    return dependent === '';
  };

  // Only re-rendered if `brand, errors` changes:
  const selectBrand = useMemo(
    () => (
      <FormikForm.Item validation={touched.brand && errors.brand}>
        <CustomSelect
          title={selectTitle('Brand', brandValue)}
          value={brandValue}
          data={carsBrand}
          onSelectValueChange={handleChangeCarsBrand}
          validation={errors.brand && touched.brand}
        />
      </FormikForm.Item>
    ),
    [brandValue, errors.brand, touched.brand],
  );

  // Only re-rendered if `model, carModels, errors` changes:
  const selectModel = useMemo(
    () => (
      <FormikForm.Item validation={touched.model && errors.model}>
        <CustomSelect
          title={selectTitle('Model', modelValue)}
          value={modelValue}
          data={carModels}
          disabled={disableSelect(brandValue)}
          onSelectValueChange={handleChangeCarModel}
          validation={errors.model && touched.model}
        />
      </FormikForm.Item>
    ),
    [
      modelValue,
      brandValue,
      carModels,
      errors.brand,
      errors.model,
      touched.model,
    ],
  );

  return (
    <React.Fragment>
      {selectBrand}
      {selectModel}
    </React.Fragment>
  );
};

export default CustomSelectCars;
