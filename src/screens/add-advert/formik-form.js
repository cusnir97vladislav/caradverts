import React, {useMemo} from 'react';
import {Button, ScrollView, Text} from 'react-native';
import {fuelType} from '../../commons/utils/dates';
import CustomInput from '../../commons/components/custom-input';
import CustomImagePicker from '../../commons/components/custom-image-picker';
import CustomSelect from '../../commons/components/custom-select';
import CustomPicker from '../../commons/components/custom-picker';
import CustomCheckBox from '../../commons/components/custom-checkbox';
import CustomSelectCars from './custom-select-cars';

const FormikItem = ({validation, children}) => {
  const showValidation = validation && (
    <Text style={{color: 'red', paddingLeft: 10}}>{validation}</Text>
  );
  return (
    <React.Fragment>
      {children && children}
      {showValidation}
    </React.Fragment>
  );
};

const FormikForm = (props) => {
  const {
    data,
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    setFieldValue,
    errors,
    touched,
  } = props;

  const selectTitle = (initialValue, value) => {
    return value !== '' ? value : initialValue;
  };

  const handleCheckBox = () => {
    setFieldValue('damage', !values.damage);
  };

  const handleUploadImage = (value) => {
    setFieldValue('damageImages', value);
  };

  const inputTitle = useMemo(
    () => (
      <FormikItem validation={touched.title && errors.title}>
        <CustomInput
          placeholder="Title"
          onChangeText={handleChange('title')}
          onBlur={handleBlur('title')}
          value={values.title}
          validation={errors.title && touched.title}
        />
      </FormikItem>
    ),
    [values.title, errors.title, touched.title],
  );

  const selectCarsGroup = useMemo(
    () => (
      <CustomSelectCars
        {...data}
        brandValue={values.brand}
        modelValue={values.model}
        onSelectBrand={handleChange('brand')}
        onSelectModel={handleChange('model')}
        validation={{errors, touched}}
      />
    ),
    [
      values.brand,
      values.model,
      errors.brand,
      errors.model,
      touched.brand,
      touched.model,
    ],
  );

  const selectFuel = useMemo(
    () => (
      <FormikItem validation={touched.fuel && errors.fuel}>
        <CustomSelect
          title={selectTitle('Fuel', values.fuel)}
          value={values.fuel}
          data={fuelType}
          onSelectValueChange={handleChange('fuel')}
          validation={errors.fuel && touched.fuel}
        />
      </FormikItem>
    ),
    [fuelType, values.fuel, errors.fuel, touched.fuel],
  );

  const selectColor = useMemo(
    () => (
      <FormikItem validation={touched.color && errors.color}>
        <CustomPicker
          value={values.color}
          onSelect={handleChange('color')}
          validation={errors.color && touched.color}
        />
      </FormikItem>
    ),
    [values.color, errors.color, touched.color],
  );

  const checkBoxDamage = useMemo(
    () => (
      <CustomCheckBox
        title="Vehicle damaged"
        value={values.damage}
        onChange={handleCheckBox}
      />
    ),
    [values.damage, errors.damage, touched.damage],
  );

  const textareaDamage = useMemo(
    () => (
      <FormikItem
        validation={touched.damageDescription && errors.damageDescription}>
        <CustomInput
          placeholder="Damage description"
          onChangeText={handleChange('damageDescription')}
          onBlur={handleBlur('damageDescription')}
          value={values.damageDescription}
          multiline={true}
          numberOfLines={3}
          validation={errors.damageDescription && touched.damageDescription}
        />
      </FormikItem>
    ),
    [
      values.damageDescription,
      errors.damageDescription,
      touched.damageDescription,
    ],
  );

  const imagesBox = useMemo(
    () => (
      <FormikItem validation={touched.damageImages && errors.damageImages}>
        <CustomImagePicker
          value={values.damageImages}
          onChange={handleUploadImage}
          validation={errors}
        />
      </FormikItem>
    ),
    [values.damageImages, errors.damageImages, touched.damageImages],
  );

  const showCheckboxContent = values.damage ? (
    <React.Fragment>
      {textareaDamage}
      {imagesBox}
    </React.Fragment>
  ) : null;

  return (
    <ScrollView>
      {inputTitle}
      {selectCarsGroup}
      {selectFuel}
      {selectColor}
      {checkBoxDamage}
      {showCheckboxContent}
      <Button onPress={handleSubmit} title="Submit" />
    </ScrollView>
  );
};

FormikForm.Item = FormikItem;
export default FormikForm;
