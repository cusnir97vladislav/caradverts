import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import CarDetails from '../screens/car-details';
import Home from '../screens/home';

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Home" component={Home} />
      <HomeStack.Screen name="Details" component={CarDetails} />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
