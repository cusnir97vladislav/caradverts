import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
Ionicons.loadFont();
import {primaryDark, primaryDefault} from '../commons/colors';
import {ICON_SIZE} from '../commons/sizes';
import HomeStackScreen from './home-stack-screen';
import AddAdvert from '../screens/add-advert';

const Tab = createBottomTabNavigator();

function NavigationStackScreen() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let iconName;
            let iconColor = focused ? primaryDefault : primaryDark;

            switch (route.name) {
              case 'Home':
                iconName = 'home';
                break;
              case 'Add Advert':
                iconName = 'add';

                break;
            }
            return (
              <Ionicons name={iconName} size={ICON_SIZE} color={iconColor} />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: primaryDefault,
          inactiveTintColor: primaryDark,
        }}>
        <Tab.Screen name="Home" component={HomeStackScreen} />
        <Tab.Screen name="Add Advert" component={AddAdvert} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default NavigationStackScreen;
